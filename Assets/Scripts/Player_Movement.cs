using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    Boolean canJump=true;
    public Boolean roll;
   
    // Start is called before the first frame update
    void Start()
    {
        roll = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("a"))
        {
            if (canJump)
            {
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200f * Time.deltaTime, 0));
                gameObject.GetComponent<Animator>().SetBool("moving", true);
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
                gameObject.GetComponent<Animator>().SetBool("jump", false);
                roll = false;
            }
            else{
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200f * Time.deltaTime, 0));
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
                roll = false;
            }
            
        }

        if (Input.GetKey("d"))
        {
            if (canJump) { 
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(200f * Time.deltaTime, 0));
                gameObject.GetComponent<Animator>().SetBool("moving", true);
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
                gameObject.GetComponent<Animator>().SetBool("jump", false);
                roll = false;
            }
            else
            {
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(200f * Time.deltaTime, 0));
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
                roll = false;
            }

        }

        if (!Input.GetKey("d")&&!Input.GetKey("a"))
        {
            gameObject.GetComponent<Animator>().SetBool("moving", false);
            roll = false;
        }

        if ((Input.GetKeyDown("w")&& canJump) || (Input.GetKeyDown("w") && Input.GetKey("d") && canJump)|| (Input.GetKeyDown("w") && Input.GetKeyDown("a") && canJump))
        {
            canJump = false;
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100f));
            gameObject.GetComponent<Animator>().SetBool("moving", false);
            gameObject.GetComponent<Animator>().SetBool("jump",true);


        }
        
        if ((Input.GetKey("a"))&&(Input.GetKeyDown("space")))
        {
            gameObject.GetComponent<Animator>().SetBool("roll", true);
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-2000f * Time.deltaTime, 0));
        }
        else if((Input.GetKey("d"))&&(Input.GetKeyDown("space")))
        {
            gameObject.GetComponent<Animator>().SetBool("roll", true);
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(2000f * Time.deltaTime, 0));
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "ground")
        {
            canJump = true;
            gameObject.GetComponent<Animator>().SetBool("jump", false);
        }
        

    }

    public void stopRoll()
    {
        gameObject.GetComponent<Animator>().SetBool("roll", false);
        roll = false;
        
    }

}

